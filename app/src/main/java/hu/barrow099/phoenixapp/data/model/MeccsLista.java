package hu.barrow099.phoenixapp.data.model;

import java.util.ArrayList;

import hu.barrow099.phoenixapp.data.model.Meccs;

/**
 * Created by Barrow099 on 2016. 10. 24..
 */

public class MeccsLista {
    private ArrayList<Meccs> meccsek;

    public MeccsLista(ArrayList<Meccs> lista) {
        meccsek = lista;
    }

    public ArrayList<Meccs> getMeccsek() {
        return meccsek;
    }

    public void setMeccsek(ArrayList<Meccs> meccsek) {
        this.meccsek = meccsek;
    }
}
