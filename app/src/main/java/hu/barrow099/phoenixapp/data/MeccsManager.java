package hu.barrow099.phoenixapp.data;


import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import hu.barrow099.phoenixapp.Config;
import hu.barrow099.phoenixapp.ErrorHelper;
import hu.barrow099.phoenixapp.Log;
import hu.barrow099.phoenixapp.R;
import hu.barrow099.phoenixapp.StorageHelper;
import hu.barrow099.phoenixapp.activity.KedvencekActivity;
import hu.barrow099.phoenixapp.data.model.Meccs;
import hu.barrow099.phoenixapp.data.model.MeccsLista;

/**
 * Készítette Magyar Máté
 *  2016. 11. 03.
 */

public class MeccsManager {
    private static MeccsManager instance;
    private ListView l;
    private Spinner s;

    private Map<String,MeccsLista> meccsMap;
    private boolean done = false;
    private MeccsLista fullList;

    public static MeccsManager getInstance() {
        return instance;
    }

    private MeccsManager(ListView l, Spinner s) {
        this.l = l;
        this.s = s;

    }

    public Map<String,MeccsLista> getMeccsMap(){
        return meccsMap;
    }

    public MeccsLista getMeccsLista(){
        checkSet();
        return fullList;
    }

    private void checkSet() {
        if(!done)
            throw new IllegalStateException("Nem lett setupolva a MeccsManager");
    }
    Activity ac;
    public static void createMeccsManager(ListView list, Spinner spinner,Activity a){
        MeccsManager m = new MeccsManager(list,spinner);
        m.ac = a;
        instance = m;
        Log.d("MM:" + instance);
        m.setup();

    }

    private String currentTeam = "Összes";

    private void setup() {
        File f = StorageHelper.getAppDir();
        File f2 = new File(f,"data");
        File meccsF = new File(f2,"meccs.bdf");
        StringBuilder data = new StringBuilder();
        String line;
        try {
            BufferedReader r = new BufferedReader(new FileReader(meccsF));
            while((line = r.readLine()) != null){
                data.append(line);
            }
            r.close();
        } catch (IOException e) {
            ErrorHelper.showExErrorMessage("Nem lehet betölteni a meccsadatbázist\nKérjük küldje el a hibajelentést a fejlesztőnek!",ac,e);

        }
        Gson gs = new GsonBuilder().setDateFormat("yyyy.MM.dd HH:mm").create();
        fullList = gs.fromJson(data.toString(),MeccsLista.class);


        meccsMap = new HashMap<>();
        meccsMap.put("Lejátszott",new MeccsLista(new ArrayList<Meccs>()));
        meccsMap.put("Összes",new MeccsLista(new ArrayList<Meccs>()));
        for (Meccs meccs : fullList.getMeccsek()) {
            if(meccs.getMeccsDatum().before(new Date())){
                Log.d(meccs.getMeccsDatum() + " || " + (new Date()).toString());
                meccsMap.get("Lejátszott").getMeccsek().add(meccs);
                Log.d("Vót már: " + meccs.toString());
            }else{
                meccsMap.get("Összes").getMeccsek().add(meccs);
                if(meccsMap.containsKey(meccs.getPhxTeam())){
                    meccsMap.get(meccs.getPhxTeam()).getMeccsek().add(meccs);
                }else{
                    MeccsLista m2 = new MeccsLista(new ArrayList<Meccs>());
                    m2.getMeccsek().add(meccs);
                    meccsMap.put(meccs.getPhxTeam(),m2);
                }
            }
        }
        ArrayList<Meccs> list = meccsMap.get("Összes").getMeccsek();

        Comparator<Meccs> meccsComparator = new Comparator<Meccs>() {
            @Override
            public int compare(Meccs o1, Meccs o2) {
                if (o1.getMeccsDatum().after(o2.getMeccsDatum())) {
                    return 1;
                } else {
                    return -1;
                }
            }
        };

        Config fConfig = new Config("kedvencek");
        fConfig.load();

        fConfig.printConfigMap();
        if(!fConfig.getConfig("kedvencek","-1").equals("-1")){
            ArrayList<String> al = new ArrayList<>();
            int c = 1;
            for(c = 1;c <=3;c++){
                String s = fConfig.getConfig("kedvenc" + c);
                if(s != null){
                    al.add(s);
                }
            }
            ArrayList<Meccs> mList = new ArrayList<>();
            for (String s1 : al) {
                MeccsLista meccsLista = meccsMap.get(s1);
                if(meccsLista != null)
                    mList.addAll(meccsLista.getMeccsek());
            }
            if(meccsMap.containsKey(""))
                mList.addAll(meccsMap.get("").getMeccsek());
            if(!mList.isEmpty()){
                meccsMap.put("Kedvencek",new MeccsLista(mList));
                this.currentTeam = "Kedvencek";
            }

        }


        updateListToTeamChange();

        String teamArray[] = new String[meccsMap.keySet().size() - 1];
        ArrayList<String> bList = CsapatHasonlito.hasonlit(meccsMap.keySet());
        Log.d(bList);
        teamArray = bList.toArray(teamArray);
        System.out.println(teamArray);
        Log.d("ITT1");
        s.setAdapter(new ArrayAdapter<>(l.getContext(),android.R.layout.simple_spinner_dropdown_item,teamArray));
        if(!meccsMap.containsKey("Kedvencek"))
            s.setSelection(((ArrayAdapter<String>)s.getAdapter()).getPosition("Kedvencek"));
        Log.d("ITT2");
        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentTeam = (String)parent.getItemAtPosition(position);
                updateListToTeamChange();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //DO NOTHING
            }
        });
        for (Map.Entry<String, MeccsLista> meccsListaEntry : meccsMap.entrySet()) {
            MeccsLista value = meccsListaEntry.getValue();
            Collections.sort(value.getMeccsek(),meccsComparator);
        }

        Log.d("ITT3");
        checkFavorites();
        Log.d("ITT4");
        done = true;
        new NotificationTask().execute(new Object[]{l.getContext()});
    }

    private void checkFavorites() {
        Config cfg = new Config("kedvencek");
        cfg.load();
        if(cfg.getConfig("kedvencek","-1").equals("-1")){
            Log.i("Nincsnek kedvencek");
            Intent i = new Intent(this.l.getContext(), KedvencekActivity.class);
            String[] data = new String[meccsMap.keySet().size()];
            data = meccsMap.keySet().toArray(data);
            i.putExtra("csapatok",data);
            i.putExtra("config",cfg);
            l.getContext().startActivity(i);
            Log.i("Vége");
        }
    }

    private void updateListToTeamChange(){
        Meccs[] mArray = new Meccs[meccsMap.get(currentTeam).getMeccsek().size()];
        mArray = meccsMap.get(currentTeam).getMeccsek().toArray(mArray);
        MeccsAdapter a = new MeccsAdapter(l.getContext(), R.layout.meccslist_row,mArray);
        l.setAdapter(a);
    }

    public String[] getCsapatLista() {
        String[] data = new String[meccsMap.keySet().size()];
        data = meccsMap.keySet().toArray(data);
        return data;
    }
}
