package hu.barrow099.phoenixapp.data;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;


import java.io.File;
import java.io.IOException;

import hu.barrow099.phoenixapp.CommonHelper;
import hu.barrow099.phoenixapp.Config;
import hu.barrow099.phoenixapp.DebugConfig;
import hu.barrow099.phoenixapp.ErrorHelper;
import hu.barrow099.phoenixapp.Log;
import hu.barrow099.phoenixapp.NetworkHelper;
import hu.barrow099.phoenixapp.StorageHelper;
import hu.barrow099.phoenixapp.notification.NotificationService;

/**
 * Created by Barrow099 on 2016. 11. 03..
 */

public class LoaderTask extends AsyncTask<Object,Void,Void> {

    Activity currentActivity;
    Class<Activity> mainActivity;
    ProgressDialog progressDialog;

    private static final String meccsURL =   "https://dl.dropboxusercontent.com/sh/bceiz1s92fk76k7/AACE0C1kIb4nzIO2qWvHG3M7a/meccs1.json";
    public static final String versionURL = "https://dl.dropboxusercontent.com/sh/bceiz1s92fk76k7/AAB3YMAkgRfwjEVrAEGuZqLUa/version";

    @Override
    protected void onPostExecute(Void aVoid) {
        CommonHelper.sleep(1000);
        if(progressDialog != null)
            progressDialog.dismiss();
        if(isMyServiceRunning(NotificationService.class,currentActivity)){
            currentActivity.stopService(new Intent(currentActivity, NotificationService.class));
        }
        Intent is = new Intent(currentActivity, NotificationService.class);
        currentActivity.startService(is);
        Intent i = new Intent(this.currentActivity,mainActivity);
        i.putExtra("normalLaunch",true);
        currentActivity.startActivity(i);
        currentActivity.finish();

    }

    private boolean isMyServiceRunning(Class<?> serviceClass,Context ctx) {
        ActivityManager manager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    //List: {CurrentActivity,Class<NextActivity>}
    @Override
    protected Void doInBackground(Object... params) {

        if(params.length != 2 || !(params[0] instanceof Activity) || !(params[1] instanceof Class)){
            throw new IllegalArgumentException("hibás argumentumok a LoaderTaskban");
        }
        Activity currentActivity = (Activity) params[0];
        Class<Activity> nextActivity;
        try{
            nextActivity = (Class<Activity>) params[1];
        }catch (ClassCastException e){
            throw new IllegalArgumentException("hibás argumentumok a LoaderTaskban");
        }

        Config c = Config.getDefaultConfig();

        boolean firstRun = false;
        if(c.getConfig("localVersion","-1").equals("-1")){
            firstRun = true;
            Log.i("Első futtatás");
        }
        if(firstRun){
            c.setConfig("debugMode","false");
            c.save();
        }
        if(Boolean.parseBoolean(c.getConfig("debugMode"))){
            DebugConfig.DEBUG_MODE = true;
        }


        this.currentActivity = currentActivity;
        this.mainActivity = nextActivity;


        if(!NetworkHelper.isNetworkAvailable(currentActivity.getApplicationContext())){
            Log.w("Nincs internet");
            if(firstRun)
                ErrorHelper.showFatalErrorMessage("Az első futtatáshoz internetkapcsolat szükséges",currentActivity);

        }
        try {
            boolean showError = false;
            String sVersion = "1";
            try {
                sVersion = NetworkHelper.readRemoteFile(versionURL, this.currentActivity);
            } catch (IOException e) {
                showError = true;
                if (firstRun)
                    ErrorHelper.showFatalErrorMessage("Az első futtatáshoz internetkapcsolat szükséges", currentActivity);
            }

            if (sVersion == null) {
                sVersion = String.valueOf(Integer.MAX_VALUE - 1);
            }
            if (sVersion.contains("\n"))
                sVersion = sVersion.split("\n")[0];
            int version = Integer.parseInt(sVersion);
            Log.i("helyiVerzió: " + c.getConfig("localVersion", "-1") + ", távoliVerzió:" + version);
            int lVersion = Integer.parseInt(c.getConfig("localVersion", "-1"));
            if (sVersion == null) {
                version = -1;
            }
            if (lVersion < version) {
                Log.i("Frissítés szükséges");
                Log.d("localVersion < remoteVersion : " + lVersion + " < " + version);
                currentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog = ProgressDialog.show(LoaderTask.this.currentActivity, "Adatbázis letöltése", "A program letölti az új meccsadatbázist", true);
                    }
                });

                File dataDir = new File(StorageHelper.getAppDir(), "data");
                StorageHelper.createDirIfNotExists(dataDir);
                File meccsFile = new File(dataDir, "meccs.bdf");
                try {
                    NetworkHelper.downloadFile(meccsURL, meccsFile);
                    c.setConfig("localVersion", version);
                    c.save();
                } catch (IOException e) {
                    showError = true;
                    if (firstRun)
                        ErrorHelper.showFatalErrorMessage("Az első futtatáshoz internetkapcsolat szükséges", currentActivity);
                }
                if (showError)
                    this.currentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LoaderTask.this.currentActivity, "Nem lehet frissíteni az adatbázist", Toast.LENGTH_SHORT).show();
                        }
                    });


            }
        }catch (Exception e){
            Log.wtf("Failed to load file",e);
            Config cc = Config.getDefaultConfig();
            /*if(Boolean.parseBoolean(cc.getConfig("failed","false"))){
                File f = new File(Environment.getExternalStorageDirectory(),"hu.barrow099.phoenixapp.old");
                StorageHelper.getAppDir().renameTo(f);
            }else {
                cc.setConfig("failed",true);
            }*/
        }

        return null;
    }

    public static boolean delete(File path) {
        boolean result = true;
        if (path.exists()) {
            if (path.isDirectory()) {
                for (File child : path.listFiles()) {
                    result &= delete(child);
                }
                result &= path.delete(); // Delete empty directory.
            }
            if (path.isFile()) {
                result &= path.delete();
            }
            if (!result) {
                Log.i("Delete failed;");
            }
            return result;
        } else {
            Log.i("File does not exist.");
            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }
}
