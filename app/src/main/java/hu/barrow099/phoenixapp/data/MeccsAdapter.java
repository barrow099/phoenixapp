package hu.barrow099.phoenixapp.data;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import hu.barrow099.phoenixapp.Log;
import hu.barrow099.phoenixapp.R;
import hu.barrow099.phoenixapp.data.model.Meccs;
import hu.barrow099.phoenixapp.data.model.MeccsLista;



/**
 * Created by Barrow099 on 2016. 11. 02..
*/

public class MeccsAdapter extends ArrayAdapter<Meccs> {

    Context context;
    int layoutResourceId;
    Meccs data[] = null;

    public MeccsAdapter(Context context, int resource, Meccs[] data) {
        super(context, resource, data);
        this.layoutResourceId = resource;
        this.context = context;
        this.data = data;
    }

    public static MeccsAdapter createMeccsAdapter(Context context, int resource, MeccsLista data){
        Meccs[] mArray = new Meccs[data.getMeccsek().size()];
        mArray = data.getMeccsek().toArray(mArray);
        return new MeccsAdapter(context,resource,mArray);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MeccsHolder holder = null;
        if(row == null){
            LayoutInflater inf = ((Activity)context).getLayoutInflater();
            row = inf.inflate(layoutResourceId,parent,false);

            holder = new MeccsHolder();
            holder.hazai = (TextView)row.findViewById(R.id.hazai);
            holder.vendeg = (TextView)row.findViewById(R.id.vendeg);
            holder.datum = (TextView)row.findViewById(R.id.datum);
            holder.helyszin = (TextView) row.findViewById(R.id.helyszin);
            row.setTag(holder);
        }
        else {
            holder = (MeccsHolder) row.getTag();
        }

        Meccs m = data[position];
        String sHazai, sVendeg;
        if(m.getHazai().startsWith("Phoenix")){
            sHazai = m.getHazai() + " " + m.getPhxTeam();
            sVendeg = m.getEllenfel();
        }else{
            sHazai = m.getHazai();
            sVendeg = m.getEllenfel() + " " + m.getPhxTeam();
        }
        holder.hazai.setText(sHazai);
        holder.vendeg.setText(sVendeg);

        String hely = "";
        if(m.getHelyszin().length() > 10){
            hely = m.getHelyszin().substring(0,7) + "...";
        }else{
            hely = m.getHelyszin();
        }
        holder.helyszin.setText(hely);
        if((m.getHazai() + " " + m.getPhxTeam() + " - " + m.getEllenfel()).length() >= 29){
            holder.hazai.setTextSize(14.0f);
            holder.vendeg.setTextSize(14.0f);
            TextView div = (TextView) row.findViewById(R.id.div);
            div.setTextSize(14.0f);
        }
        DateFormat df = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

        holder.datum.setText(new SimpleDateFormat("yyyy.MM.dd").format(m.getMeccsDatum()) + " " + df.format(m.getMeccsDatum()));

        return row;
    }

    private static class MeccsHolder{
        TextView hazai;
        TextView vendeg;
        TextView datum;
        TextView helyszin;
    }
}
