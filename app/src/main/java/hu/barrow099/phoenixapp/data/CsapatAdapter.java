package hu.barrow099.phoenixapp.data;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import hu.barrow099.phoenixapp.R;
import hu.barrow099.phoenixapp.data.model.Csapat;

/**
 * Készítette Magyar Máté
 * 2016. 11. 05.
 */

public class CsapatAdapter extends ArrayAdapter<Csapat> {

    Context context;
    int layoutResourceId;
    public Csapat[] data = null;

    public CsapatAdapter(Context context, int resource,Csapat[] data) {
        super(context, resource, data);
        this.layoutResourceId = resource;
        this.context = context;
        this.data = data;
    }

    private int favCount = 0;

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CsapatHolder holder = null;
        if(row == null){
            LayoutInflater inf = ((Activity)context).getLayoutInflater();
            row = inf.inflate(layoutResourceId,parent,false);

            holder = new CsapatHolder();
            holder.csapatNev = (TextView)row.findViewById(R.id.rowTeamName);
            holder.kedvencCB = (CheckBox) row.findViewById(R.id.rowCB);
            holder.bg = (LinearLayout) row.findViewById(R.id.rowBG);
            row.setTag(holder);
        }else{
            holder = (CsapatHolder) row.getTag();
        }
        final Csapat cs = data[position];
        holder.csapatNev.setText(cs.getName());
        holder.kedvencCB.setSelected(cs.isSelected());
        final CsapatHolder fHolder = holder;
        holder.kedvencCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked)
                    cs.setSelected(false);
                if(isChecked){
                    if(favCount < 3){
                        cs.setSelected(isChecked);
                        favCount += 1;
                    }else {
                        showWarn(fHolder.csapatNev);
                        fHolder.kedvencCB.setChecked(false);
                    }
                }else
                    favCount -= 1;

            }
        });
        holder.csapatNev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fHolder.kedvencCB.setChecked(!fHolder.kedvencCB.isChecked());
            }
        });
        holder.bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fHolder.kedvencCB.setChecked(!fHolder.kedvencCB.isChecked());
            }
        });
        return row;
    }

    private void showWarn(View v) {
        AlertDialog alertDialog = new AlertDialog.Builder(v.getContext()).create();
        alertDialog.setTitle("Hiba");
        alertDialog.setMessage("Csak három kedvencet csapatot választhat");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Rendben",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    static class CsapatHolder{
        TextView csapatNev;
        CheckBox kedvencCB;
        LinearLayout bg;
    }
}
