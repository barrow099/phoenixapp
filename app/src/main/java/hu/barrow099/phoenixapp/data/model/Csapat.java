package hu.barrow099.phoenixapp.data.model;

/**
 * Készítette Magyar Máté
 * 2016. 11. 05.
 */

public class Csapat {
    private String name;
    private boolean selected;

    public Csapat(String name,boolean selected) {
        this.name = name; this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
