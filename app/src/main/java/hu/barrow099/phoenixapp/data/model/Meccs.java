package hu.barrow099.phoenixapp.data.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Barrow099 on 2016. 10. 24..
 */
public class Meccs implements Serializable{
    private String hazai;
    private String ellenfel;
    private String phxTeam;
    private String helyszin;
    private Date   meccsDatum;

    public Meccs(String hazai, String ellenfel, String phxTeam, Date meccsDatum,String helyszin) {
        this.hazai = hazai;
        this.ellenfel = ellenfel;
        this.phxTeam = phxTeam;
        this.meccsDatum = meccsDatum;
        this.helyszin = helyszin;
    }

    public String getHelyszin() {
        return helyszin;
    }

    public void setHelyszin(String helyszin) {
        this.helyszin = helyszin;
    }

    public String getPhxTeam() {
        return phxTeam;
    }

    public void setPhxTeam(String phxTeam) {
        this.phxTeam = phxTeam;
    }

    public Date getMeccsDatum() {
        return meccsDatum;
    }

    public void setMeccsDatum(Date meccsDatum) {
        this.meccsDatum = meccsDatum;
    }

    public String getHazai() {
        return hazai;
    }

    public void setHazai(String hazai) {
        this.hazai = hazai;
    }

    public String getEllenfel() {
        return ellenfel;
    }

    public void setEllenfel(String ellenfel) {
        this.ellenfel = ellenfel;
    }

    @Override
    public String toString() {
        if(getHazai().startsWith("Phoenix"))
            return getHazai() + " " + getPhxTeam() + " - " + getEllenfel();
        else
            return getHazai() + " - " + getEllenfel()+ " " + getPhxTeam() ;
    }
}
