package hu.barrow099.phoenixapp.data;

/**
 * Készítette Magyar Máté
 * 2016. 12. 18.
 */
public class CsapatComp implements java.util.Comparator<String> {

    @Override
    public int compare(String o1, String o2) {
        if(o1.equals("Kedvencek") || o1.equals("Lejátszott") || o1.equals("Összes"))
            return sortBase(o1,o2);
        else {
            if(o1.startsWith("U")){
                if(o2.startsWith("U")){
                    return o1.compareTo(o2);
                }else if(o2.startsWith("Női")){
                    return -1;
                }
                else if(o2.equals("Kedvencek") || o2.equals("Lejátszott") || o2.equals("Összes")){
                    return 1;
                }
                else{
                    return -1;
                }
            }
            else if(o1.startsWith("Női")){
                if(o2.startsWith("Női")){
                    return o1.compareTo(o2);
                }else if(o2.startsWith("U")){
                    return 1;
                }
                else if(o2.equals("Kedvencek") || o2.equals("Lejátszott") || o2.equals("Összes")){
                    return 1;
                }
                else{
                    return 1;
                }
            }
            else{
                if(o2.startsWith("Női")){
                    return -1;
                }else if(o2.startsWith("U")){
                    return 1;
                }
                else if(o2.equals("Kedvencek") || o2.equals("Lejátszott") || o2.equals("Összes")){
                    return 1;
                }else{
                    return o1.compareTo(o2);
                }
            }
        }
    }

    private int sortBase(String o1, String o2) {
        if(o1.equals("Kedvencek")){
            switch (o2){
                case "Lejátszott": return -1;
                case "Összes": return -1;
                default: return -1;
            }
        }
        else if(o1.equals("Lejátszott")){
            switch (o2){
                case "Kedvencek": return 1;
                case "Összes": return 1;
                default: return -1;
            }
        }
        else if(o1.equals("Összes")){
            switch (o2){
                case "Lejátszott": return -1;
                case "Kedvencek": return 1;
                default: return -1;
            }
        }
        return 0;
    }
}
