package hu.barrow099.phoenixapp;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Environment;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Created by Barrow099 on 2016. 11. 03..
 */

public class Config implements Serializable{
    //Vars
    private String configName = "default";
    //TODO make it private
    public Map<String,String> configMap;
    private File cfgFile = null;
    private String pName = "";


    //Static vars
    private static Config defConfig = null;

    //Constructors
    private Config() {
        cfgList.add(this);
        this.pName = configName + new Random().nextInt();
    }
    public Config(String configName) {
        this.configName = configName;
        cfgList.add(this);
        this.pName = configName + new Random().nextInt();
    }

    public synchronized void printConfigMap(){
        Set<Map.Entry<String, String>> entries = configMap.entrySet();
        Log.i("CONFIG MAP: " + pName);
        for (Map.Entry<String, String> entry : entries) {
            Log.i("\t" + entry.getKey() + "=" + entry.getValue());
        }

    }

    //Class body
    public synchronized void load(){
        configMap = new HashMap<>();
        if(cfgmap.containsKey(this.configName)){
            configMap = cfgmap.get(this.configName).configMap;
            cfgFile = cfgmap.get(this.configName).cfgFile;
            pName = cfgmap.get(this.configName).pName;
            Log.d(configMap.hashCode());
            Log.d("gyorsítótárazott Config: " + pName);
            return;
        }else {
            cfgmap.put(this.configName,this);
        }

        //Load cfg from file
        try{

            if(!StorageHelper.isExternalStorageAvailable()){
                throw new IOException("Nem lehet betölteni a configurációt");
            }

            File baseDir = Environment.getExternalStorageDirectory();
            File appDir = new File(baseDir,"hu.barrow099.phoenixapp");
            File cfgDir = new File(appDir,"config");

            StorageHelper.createDirIfNotExists(appDir);
            StorageHelper.createDirIfNotExists(cfgDir);

            cfgFile = new File(cfgDir,configName + ".cfg");
            if(!cfgFile.exists()){
                Log.i("Üres config: " + pName);
                return;
            }

            BufferedReader cfgReader = new BufferedReader(new FileReader(cfgFile));
            String currentLine = null;
            while((currentLine = cfgReader.readLine()) != null){
                //Process line
                if(currentLine.trim().equals("") || currentLine.startsWith("#") || !currentLine.contains("=")){
                    //Blank, bad or comment line
                    continue;
                }
                String kv[] = currentLine.split("=");
                if(kv.length == 1){
                    //Null value
                    configMap.put(kv[0],null);
                }else {
                    configMap.put(kv[0],kv[1]);
                }
            }
            cfgReader.close();
            Log.i("Config \'" + pName + "\' betöltve");
        }catch (IOException e){
            Log.wtf("Nem lehet beolvasni a konfigurációt",e);
        }
    }
    public synchronized void save() {
        try {
            if (!StorageHelper.isExternalStorageAvailable()) {
                throw new IOException("Nem lehet betölteni a configurációt");
            }

            File baseDir = Environment.getExternalStorageDirectory();
            File appDir = new File(baseDir, "hu.barrow099.phoenixapp");
            File cfgDir = new File(appDir, "config");

            StorageHelper.createDirIfNotExists(appDir);
            StorageHelper.createDirIfNotExists(cfgDir);

            if(cfgFile == null){
                throw new IllegalStateException("Nem lett meghívva a load() erre a Config példányra");
            }
            BufferedWriter cfgWriter = new BufferedWriter(new FileWriter(cfgFile));
            cfgWriter.write("#NE MÓDOSÍTSA ezt a fájlt\n");
            Set<Map.Entry<String, String>> entries = configMap.entrySet();
            for (Map.Entry<String, String> entry : entries) {
                cfgWriter.write(entry.getKey() + "=" + (entry.getValue() == null ? "" : entry.getValue())  + "\n");
            }
            cfgWriter.flush();
            cfgWriter.close();
            Log.i("Config \'" + pName + "\' elmentve");
        }catch (IOException e){
            Log.wtf("Nem lehet beolvasni a konfigurációt",e);
        }
    }

    public synchronized void setConfig(String key,Object value){
        if(cfgFile == null){
            throw new IllegalStateException("Nem lett meghívva a load() erre a Config példányra");
        }
        if(configMap.containsKey(key)){
            configMap.remove(key);
        }
        if(value == null){
            configMap.put(key,null);
        }else{
            configMap.put(key,value.toString());
        }


        Log.d("Config hozzáadva: " + key + "=" + value);
    }
    public synchronized String getConfig(String key){
        if(cfgFile == null){
            throw new IllegalStateException("Nem lett meghívva a load() erre a Config példányra");
        }
        if(!configMap.containsKey(key)){
            return null;
        }
        return configMap.get(key);
    }
    public synchronized String getConfig(String key,String defaultValue){
        String s = getConfig(key);
        if(s == null){
            return defaultValue;
        }
        return s;
    }

    //Static functions
    public static Config getDefaultConfig(){
        if(defConfig == null){
            defConfig = new Config();
            defConfig.load();
        }
        return defConfig;
    }

    private static ArrayList<Config> cfgList = new ArrayList<>();
    private static Map<String,Config> cfgmap = new HashMap<>();
    public static void debugDelAll(final Context ctx) {

            for (Config config : cfgList) {
                try {
                    config.cfgFile.delete();
                }catch (Exception e){
                    Log.w("Nem lehet törölni az egyik configurációt");
                }
            }
        ((Activity)ctx).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ctx,"A konfiguráció törölve",Toast.LENGTH_SHORT).show();
            }
        });



    }
}
