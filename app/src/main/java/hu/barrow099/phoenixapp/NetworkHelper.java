package hu.barrow099.phoenixapp;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

/**
 * Created by Barrow099 on 2016. 11. 03..
 */

public class NetworkHelper {
    public static boolean isNetworkAvailable(Context cContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) cContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String readRemoteFile(String sURL, Activity currentActivity) throws IOException{
        try {
            URL version = new URL(sURL);
            String localFile = String.valueOf(new Random().nextLong());
            BufferedInputStream bis = new BufferedInputStream(version.openStream());
            FileOutputStream fis = new FileOutputStream(new File(StorageHelper.getTempDir(),"temp" + localFile));
            byte[] buffer = new byte[1024];
            int count = 0;
            while ((count = bis.read(buffer, 0, 1024)) != -1) {
                fis.write(buffer, 0, count);
            }
            fis.close();
            bis.close();
            BufferedReader reader = new BufferedReader(new FileReader(new File(StorageHelper.getTempDir(),"temp" + localFile)));
            StringBuilder txt = new StringBuilder();
            String line = null;
            while((line = reader.readLine()) != null){
                txt.append(line + "\n");
            }
            reader.close();
            Log.i("Fájl beolvasva: " + sURL);
            new File(StorageHelper.getTempDir(),"temp" + localFile).deleteOnExit();
            return txt.toString();
        } catch (MalformedURLException e) {
           Log.w("Hibás URL");
        }
        return null;
    }

    public static void downloadFile(String surl, File meccsFile) throws IOException{
        try {
            URL version = new URL(surl);

            BufferedInputStream bis = new BufferedInputStream(version.openStream());
            FileOutputStream fis = new FileOutputStream(meccsFile);
            byte[] buffer = new byte[1024];
            int count = 0;
            while ((count = bis.read(buffer, 0, 1024)) != -1) {
                fis.write(buffer, 0, count);
            }
            fis.close();
            bis.close();
            Log.i("Fájl letöltve: " + meccsFile.getName());
        } catch (MalformedURLException e) {
            Log.w("Hibás URL");
        }
    }
}
