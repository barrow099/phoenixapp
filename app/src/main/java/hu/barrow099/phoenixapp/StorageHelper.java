package hu.barrow099.phoenixapp;

import android.os.Environment;

import java.io.File;

/**
 * Created by Barrow099 on 2016. 11. 03..
 */

public class StorageHelper {
    public static  boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static void createDirIfNotExists(File dir2create){
        if(!dir2create.exists())
            dir2create.mkdir();
    }

    public static File getAppDir(){
        if(isExternalStorageAvailable()){
            File base = Environment.getExternalStorageDirectory();
            File appDir = new File(base,"hu.barrow099.phoenixapp");
            createDirIfNotExists(appDir);
            return appDir;
        }
        throw new RuntimeException("ExternalStorage nem elérhető");

    }

    public static void deleteTempDir(){
        File tDir = getTempDir();
        for(File f : tDir.listFiles())
            f.delete();
    }

    public static File getTempDir(){
        if(isExternalStorageAvailable()){
            File base = Environment.getExternalStorageDirectory();
            File appDir = new File(base,"hu.barrow099.phoenixapp");
            File tempDir = new File(appDir,"temp");
            createDirIfNotExists(appDir);
            createDirIfNotExists(tempDir);
            return tempDir;
        }
        throw new RuntimeException("ExternalStorage nem elérhető");

    }
}
