package hu.barrow099.phoenixapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import hu.barrow099.phoenixapp.activity.CrashActivity;

/**
 * Created by Barrow099 on 2016. 11. 03..
 */

public class ErrorHelper implements Thread.UncaughtExceptionHandler {

    public static void showErrorMessage(final String message,final Activity a){
        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog alertDialog = new AlertDialog.Builder(a).create();
                alertDialog.setTitle("Hiba");
                alertDialog.setMessage(message);
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                            }
                        });
                alertDialog.show();
            }
        });

    }
    public static void showFatalErrorMessage(final String message,final Activity a){
        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog alertDialog = new AlertDialog.Builder(a).create();
                alertDialog.setTitle("Hiba");
                alertDialog.setMessage(message);
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Bezárás",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                System.exit(-1);
                            }
                        });
                alertDialog.show();
            }
        });

        while(true){
            CommonHelper.sleep(100);
        }
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        Log.wtf("Uncaught exception",e);
        try {
            StringBuilder text = new StringBuilder();
            text.append("A program összeomlott egy ismeretlen hiba miatt\n" +
                    "Kérem küldje el ezt a hibajelentést a phoenixapp@barrow099.hu címre!\n\n");
            text.append("Uncaught exception in thread: " + t.getName() + "\n");
            Writer w = new StringWriter();
            PrintWriter pw = new PrintWriter(w);
            e.printStackTrace(pw);
            pw.close();
            text.append(w.toString());
            Throwable cause = e;
            while((cause = cause.getCause()) != null){
                text.append("\nCaused by:\n");
                Writer w2 = new StringWriter();
                PrintWriter pw2 = new PrintWriter(w2);
                cause.printStackTrace(pw2);
                pw2.close();
                text.append(w2.toString());
            }
            text.append("LOG: \n");
            Log.close();
            BufferedReader r = new BufferedReader(new FileReader(new File(StorageHelper.getAppDir(),"log.txt")));
            while(r.ready()){
                text.append(r.readLine() + "\n");
            }
            r.close();
            if(CacheContext.getContext() == null)
                System.exit(-1);
            Intent i = new Intent(CacheContext.getContext(), CrashActivity.class);
            i.putExtra("text", text.toString());
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            CacheContext.getContext().startActivity(i);
            System.exit(0);
        }catch (Throwable tc){

            android.util.Log.wtf("Végzetes hiba",tc);
        }
    }

    public static void showExErrorMessage(final String s,final Activity ac,final Exception e) {
        ac.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog alertDialog = new AlertDialog.Builder(ac).create();
                alertDialog.setTitle("Hiba");
                alertDialog.setMessage(s);
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Log.wtf("Hiba:",e);
                                throw new RuntimeException("Application terminated");
                            }
                        });
                alertDialog.show();
            }
        });
    }
    /*
    Log.wtf("Hiba:",e);
            throw new RuntimeException("Application terminated");
     */
}
