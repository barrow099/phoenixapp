package hu.barrow099.phoenixapp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Barrow099 on 2016. 11. 03..
 */

public class Log {

    static PrintWriter logWriter = null;
    static{
        try {
            new File(StorageHelper.getAppDir(),"log.txt").delete();
            logWriter = new PrintWriter(new FileWriter(new File(StorageHelper.getAppDir(),"log.txt")));
        } catch (IOException e) {
            new ErrorHelper().uncaughtException(Thread.currentThread(),e);
        }
    }
    public static void close(){
        logWriter.close();
    }
    private static final String TAG = "phoenixLog";

    public static void i(Object msg){
        android.util.Log.i(TAG,msg.toString());
            logWriter.println("I:"+msg.toString());
            logWriter.flush();
    }
    public static void d(Object msg){
        android.util.Log.d(TAG,msg.toString());

            logWriter.println("D:"+msg.toString());
            logWriter.flush();

    }
    public static void w(Object msg){
        android.util.Log.w(TAG,msg.toString());

            logWriter.println("W:"+msg.toString());
            logWriter.flush();

    }
    public static void wtf(Object msg,Throwable t){
        android.util.Log.wtf(TAG,msg.toString(),t);

            logWriter.println("ERROR\n");
            t.printStackTrace(logWriter);
            t.printStackTrace();
            logWriter.flush();

    }

    public static void wtfs(String s, IOException e) {
        wtf(s,e);
        throw new RuntimeException("APPLICATION TERMINATED");
    }
}
