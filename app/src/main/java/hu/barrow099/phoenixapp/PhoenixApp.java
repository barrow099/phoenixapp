package hu.barrow099.phoenixapp;

/**
 * Készítette Magyar Máté
 * 2016. 11. 08.
 */

public class PhoenixApp {
    public static final int version = 10200;
    public static final String sVersion = "1.0.2";
    public static final boolean testVersion = false;
    public static final boolean alwaysShowNotif = false;
}
