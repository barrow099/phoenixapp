package hu.barrow099.phoenixapp.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.io.File;

import hu.barrow099.phoenixapp.CacheContext;
import hu.barrow099.phoenixapp.ErrorHelper;
import hu.barrow099.phoenixapp.Log;
import hu.barrow099.phoenixapp.PhoenixApp;
import hu.barrow099.phoenixapp.R;
import hu.barrow099.phoenixapp.StorageHelper;
import hu.barrow099.phoenixapp.data.LoaderTask;

import static hu.barrow099.phoenixapp.data.LoaderTask.delete;

public class SplashActivity extends AppCompatActivity {

    private static boolean permission = false;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i("--------------------------------------------------------------------------------");
        Thread.setDefaultUncaughtExceptionHandler(new ErrorHelper());
        setContentView(R.layout.activity_splash);
        TextView test = (TextView) findViewById(R.id.splashTEST);
        if(!PhoenixApp.testVersion){
            test.setVisibility(View.INVISIBLE);
        }
        TextView version = (TextView) findViewById(R.id.splashVersion);
        version.setText(PhoenixApp.sVersion);
        CacheContext.setContext(getApplicationContext());
        Thread dThread = new Thread(new Runnable() {
            @Override
            public void run() {
                File o = new File(Environment.getExternalStorageDirectory(),"hu.barrow099.phoenixapp.old");
                File f = StorageHelper.getAppDir();
                f.renameTo(o);
                delete(o);
                new LoaderTask().execute(new Object[]{SplashActivity.this,MainActivity.class});
            }
        });
        dThread.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(checkMyPermission()){
            Log.i("--------------------------------------------------------------------------------");
            Thread.setDefaultUncaughtExceptionHandler(new ErrorHelper());
            setContentView(R.layout.activity_splash);
            TextView test = (TextView) findViewById(R.id.splashTEST);
            if(!PhoenixApp.testVersion){
                test.setVisibility(View.INVISIBLE);
            }
            TextView version = (TextView) findViewById(R.id.splashVersion);
            version.setText(PhoenixApp.sVersion);
            CacheContext.setContext(getApplicationContext());
            Thread dThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    /*File o = new File(Environment.getExternalStorageDirectory(),"hu.barrow099.phoenixapp.old");
                    File f = StorageHelper.getAppDir();
                    f.renameTo(o);
                    delete(o);
                    */
                    new LoaderTask().execute(new Object[]{SplashActivity.this,MainActivity.class});
                }
            });
            dThread.start();
        }else{

        }

    }

    private boolean checkMyPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            } else
                return true;
        }
        return true;
    }


}
