package hu.barrow099.phoenixapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import hu.barrow099.phoenixapp.Config;
import hu.barrow099.phoenixapp.DebugConfig;
import hu.barrow099.phoenixapp.Log;
import hu.barrow099.phoenixapp.R;
import hu.barrow099.phoenixapp.data.MeccsManager;

public class AboutActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                overridePendingTransition(R.anim.rev_slide_in, R.anim.rev_slide_out);
                return true;
            default: return true;
        }
    }
    private int ccount = 0;
    private long lTime = System.currentTimeMillis();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        setContentView(R.layout.activity_about);

        TextView tw = (TextView) findViewById(R.id.details_lic);
        tw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://dl.dropboxusercontent.com/s/y1g5vd4f1fgm2x9/lic.txt?dl=0"));
                startActivity(browserIntent);
            }
        });

        ImageView imageView = (ImageView) findViewById(R.id.about_logo);


        /*imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DebugConfig.DEBUG_MODE){
                   Toast.makeText(AboutActivity.this,"Már fejlesztői módban van a program",Toast.LENGTH_SHORT).show();
                    return;
                }
                //Log.d("Hit");
                long time = System.currentTimeMillis();
                //Log.d(time -lTime);
                if((time - lTime) < 2000){
                    ccount++;
                }else {
                    ccount = 1;
                }
                switch (ccount){
                    case 4:
                        showT(4);
                        break;
                    case 5:
                        showT(5);
                        break;
                    case 6:
                        showT(6);
                        break;
                    case 7:
                        ccount = 0;
                        showT(7);
                        break;

                }
                lTime = time;
            }
        });*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private Toast lastToast = null;
    private void showT(int i) {
        if(lastToast != null)
            lastToast.cancel();
        if(DebugConfig.DEBUG_MODE){
            lastToast = Toast.makeText(this,"Már fejlesztői módban van a program",Toast.LENGTH_SHORT);
            lastToast.show();
            return;
        }
        if(i == 7){
            Config.getDefaultConfig().setConfig("debugMode","true");
            Config.getDefaultConfig().save();
            lastToast = Toast.makeText(this,"Fejlesztői módba lépett\nÍndítsa újra a programot a változtatások alkalmazásához",Toast.LENGTH_LONG);
            lastToast.show();
            return;
        }
        lastToast = Toast.makeText(this,"Még " + String.valueOf(7-i) + " lépés a fejlesztői módig",Toast.LENGTH_SHORT);
        lastToast.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.rev_slide_in, R.anim.rev_slide_out);
    }
}
