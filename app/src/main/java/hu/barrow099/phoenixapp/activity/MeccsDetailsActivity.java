package hu.barrow099.phoenixapp.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;

import hu.barrow099.phoenixapp.R;
import hu.barrow099.phoenixapp.data.model.Meccs;

public class MeccsDetailsActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                overridePendingTransition(R.anim.rev_slide_in, R.anim.rev_slide_out);
                return true;
            default: return true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        setContentView(R.layout.activity_meccs_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        if(extras == null){
            return;
        }
        final Meccs m = (Meccs)extras.get("meccs");
        TextView dHazai = (TextView)findViewById(R.id.dHazai);
        TextView dVendeg = (TextView)findViewById(R.id.dVendeg);
        TextView dTime = (TextView)findViewById(R.id.dTime);
        TextView dDate = (TextView)findViewById(R.id.dDate);
        TextView dHely = (TextView) findViewById(R.id.dHely);

        if(m.getHazai().startsWith("Phoenix")){
            dHazai.setText(m.getHazai() + " " + m.getPhxTeam());
            dVendeg.setText(m.getEllenfel());
        }else{
            dHazai.setText(m.getHazai());
            dVendeg.setText(m.getEllenfel() + " " + m.getPhxTeam());
        }
        dDate.setText(new SimpleDateFormat("yyyy.MM.dd").format(m.getMeccsDatum()));
        dTime.setText(new SimpleDateFormat("HH.mm").format(m.getMeccsDatum()));
        SpannableString content = new SpannableString(m.getHelyszin()+ " - Térkép");
        content.setSpan(new UnderlineSpan(),0,(m.getHelyszin()+ " - Térkép").length(),0);
        dHely.setText(content);
        dHely.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String uri = "geo:0,0?q=";
                String[] strings = m.getHelyszin().split(" ");
                int i = 0;
                for(String s : strings){
                    if(i++ == strings.length - 1){
                        uri += s;
                    }else{
                        uri += s + "+";
                    }
                }
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        });
        dHely.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("location", m.getHelyszin());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(MeccsDetailsActivity.this,"Másolva",Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.rev_slide_in, R.anim.rev_slide_out);
    }
}
