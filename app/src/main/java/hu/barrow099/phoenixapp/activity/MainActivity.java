package hu.barrow099.phoenixapp.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import hu.barrow099.phoenixapp.Config;
import hu.barrow099.phoenixapp.DebugConfig;
import hu.barrow099.phoenixapp.Log;
import hu.barrow099.phoenixapp.NetworkHelper;
import hu.barrow099.phoenixapp.PhoenixApp;
import hu.barrow099.phoenixapp.R;
import hu.barrow099.phoenixapp.StorageHelper;
import hu.barrow099.phoenixapp.data.LoaderTask;
import hu.barrow099.phoenixapp.data.MeccsManager;
import hu.barrow099.phoenixapp.data.model.Meccs;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class MainActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_action_menu,menu);

        MenuItem itemAbout = menu.findItem(R.id.item_about);
        itemAbout.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(MainActivity.this,AboutActivity.class);
                startActivityForResult(intent,0);
                return false;
            }
        });
        MenuItem itemCrash = menu.findItem(R.id.item_crash);

        itemCrash.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                throw new IllegalArgumentException("Debug crash");

            }
        });

        MenuItem itemCFGDEL = menu.findItem(R.id.item_cfgdel);
        itemCFGDEL.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Config.debugDelAll(MainActivity.this);
                Toast.makeText(getApplicationContext(),"A konfiguráció kitörölve",Toast.LENGTH_LONG).show();
                return false;
            }
        });

        MenuItem itemExitdev = menu.findItem(R.id.item_exitdev);
        itemExitdev.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Config.getDefaultConfig().setConfig("debugMode","false");
                Config.getDefaultConfig().save();
                DebugConfig.DEBUG_MODE = false;
                Log.i("Exit dev mode");
                Toast.makeText(getApplicationContext(),"Kilépve a fejlesztői módból",Toast.LENGTH_LONG).show();
                return false;
            }
        });
        MenuItem itemSettings = menu.findItem(R.id.item_settings);
        itemSettings.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(MainActivity.this,SettingsActivity.class));
                return false;
            }
        });
        if(!DebugConfig.DEBUG_MODE) {
            itemCrash.setVisible(false);
            itemCFGDEL.setVisible(false);
            itemExitdev.setVisible(false);
        }


        return true;
    }

    private static MainActivity instance;

    public static void ujraindit(Context context) {
        Intent intent = new Intent(context, SplashActivity.class);
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        if (context instanceof Activity) {
            ((Activity) context).finish();
        }
        Runtime.getRuntime().exit(0);
    }

    private ListView l;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getIntent().getExtras() == null && instance == null){
            /*Intent intent = new Intent(this, SplashActivity.class);
            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            System.exit(0);*/
            ujraindit(this);
        }

        instance = this;

        if(getIntent().getExtras() != null) {
            if(getIntent().getExtras().containsKey("normalLaunch"))
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            else if(getIntent().getExtras().containsKey("RESTART")){
                ujraindit(this);
            }
        }

        setContentView(R.layout.activity_main);
        l = (ListView)findViewById(R.id.meccsList);


        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Meccs m = (Meccs) parent.getItemAtPosition(position);
                Intent i = new Intent(MainActivity.this,MeccsDetailsActivity.class);
                i.putExtra("meccs",m);
                startActivity(i);
            }
        });
        remakeList();

        //AlertDialog
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.d("Loaded");
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String version = Config.getDefaultConfig().getConfig("version", "-1");
                        int vInt = Integer.parseInt(version);
                        Log.i(vInt);
                        if(PhoenixApp.version > vInt){
                            android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(MainActivity.this).create();
                            alertDialog.setTitle("Új programverzió");
                            String message = "ERROR";
                            InputStream inputStream = getResources().openRawResource(R.raw.changelog);
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            int i;
                            try {
                                i = inputStream.read();
                                while (i != -1)
                                {
                                    byteArrayOutputStream.write(i);
                                    i = inputStream.read();
                                }
                                inputStream.close();
                                message = byteArrayOutputStream.toString();
                                Log.d(message);
                            } catch (IOException e) {

                                e.printStackTrace();
                            }
                            alertDialog.setMessage(message);
                            alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "Elrejtés",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();

                                        }
                                    });
                            alertDialog.show();
                            doKeepDialog(alertDialog);
                            Config.getDefaultConfig().setConfig("version",PhoenixApp.version);
                            Config.getDefaultConfig().save();
                            StorageHelper.deleteTempDir();
                        }
                    }
                });
                boolean notifShowed = false;
                while (true) {
                    String sVersion = "1";
                    Config c = Config.getDefaultConfig();
                    try{
                        sVersion  = NetworkHelper.readRemoteFile(LoaderTask.versionURL,MainActivity.this);
                        if(sVersion ==  null){
                            sVersion = String.valueOf(Integer.MAX_VALUE - 1);
                        }
                        if(sVersion.contains("\n"))
                            sVersion = sVersion.split("\n")[0];
                        int version = Integer.parseInt(sVersion);
                        Log.i("helyiVerzió: " + c.getConfig("localVersion","-1") + ", távoliVerzió:" + version);
                        int lVersion = Integer.parseInt(c.getConfig("localVersion","-1"));
                        if(sVersion == null){
                            version = -1;
                        }
                        if(lVersion < version && !notifShowed) {
                            String notifText = "Új meccsadatbázis elérhető\nIde kattintva a program letölti azt.";
                            NotificationCompat.Builder mBuilder =
                                    new NotificationCompat.Builder(MainActivity.this);
                            mBuilder.setSmallIcon(R.drawable.notification_icon)
                                    .setContentTitle("Új meccsadatbázis")
                                    .setTicker("Új meccsadatbázis")
                                    .setContentText(notifText)
                                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.logo_t))
                                    .setDefaults(Notification.DEFAULT_ALL)
                                    .setAutoCancel(true)
                                    .setStyle(new NotificationCompat.BigTextStyle().bigText(notifText));

                            Intent resultIntent = new Intent(MainActivity.this, MainActivity.class);
                            resultIntent.putExtra("RESTART", "true");
                            TaskStackBuilder stackBuilder = TaskStackBuilder.create(MainActivity.this);
                            stackBuilder.addParentStack(MainActivity.class);
                            stackBuilder.addNextIntent(resultIntent);
                            PendingIntent resultPendingIntent =
                                    stackBuilder.getPendingIntent(
                                            0,
                                            PendingIntent.FLAG_UPDATE_CURRENT
                                    );
                            mBuilder.setContentIntent(resultPendingIntent);
                            android.app.NotificationManager mNotificationManager =
                                    (android.app.NotificationManager) MainActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
                            mNotificationManager.notify(2, mBuilder.build());
                            notifShowed = true;
                        }
                    }catch (IOException ex){
                        Log.w("Nem lehetett ellenorizni a frissiteseket: " + ex.getMessage());
                    }
                    try{
                        Thread.sleep(1000*60*2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }


            }
        }).start();
    }

    // Prevent dialog dismiss when orientation changes
    private static void doKeepDialog(Dialog dialog){
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
    }

    public static void remakeList(){
        MeccsManager.createMeccsManager(instance.l,(Spinner)instance.findViewById(R.id.csapatSpinner),instance);
    }


}
