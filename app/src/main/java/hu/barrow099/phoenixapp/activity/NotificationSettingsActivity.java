package hu.barrow099.phoenixapp.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;

import hu.barrow099.phoenixapp.Config;
import hu.barrow099.phoenixapp.R;
import hu.barrow099.phoenixapp.notification.NotificationManager;

public class NotificationSettingsActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                reloadService();
                this.finish();
                overridePendingTransition(R.anim.rev_slide_in, R.anim.rev_slide_out);
                return true;
            default: return true;
        }
    }

    private void reloadService() {
        new NotificationManager().updateNotificationsForChangedFav();
    }

    Config notifConfig;

    Switch d1;
    Switch d3;
    Switch w1;
    Switch w2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R .layout.activity_notification_settings);

        notifConfig = new Config("notifications");
        notifConfig.load();

        loadSwitches();
        createDisablingActionListener();
        loadSwitchStates();
        createSwitchActionListeners();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }

    private void createSwitchActionListeners() {
        d1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                notifConfig.setConfig("notifday1",isChecked);
                notifConfig.save();
            }
        });
        d3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                notifConfig.setConfig("notifday3",isChecked);
                notifConfig.save();
            }
        });
        w1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                notifConfig.setConfig("notifweek1",isChecked);
                notifConfig.save();
            }
        });
        w2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                notifConfig.setConfig("notifweek2",isChecked);
                notifConfig.save();
            }
        });
    }

    private void loadSwitchStates() {
        boolean bd1 = Boolean.parseBoolean(notifConfig.getConfig("notifday1","false"));
        boolean bd3 = Boolean.parseBoolean(notifConfig.getConfig("notifday3","true"));
        boolean bw1 = Boolean.parseBoolean(notifConfig.getConfig("notifweek1","false"));
        boolean bw2 = Boolean.parseBoolean(notifConfig.getConfig("notifweek2","false"));

        d1.setChecked(bd1);
        d3.setChecked(bd3);
        w1.setChecked(bw1);
        w2.setChecked(bw2);
    }

    private void loadSwitches() {
        d1 = (Switch) findViewById(R.id.nSettings_1day);
        d3 = (Switch) findViewById(R.id.nSettings_3days);
        w1 = (Switch) findViewById(R.id.nSettings_1week);
        w2 = (Switch) findViewById(R.id.nSettings_2weeks);
    }

    private void createDisablingActionListener() {
        Switch sw = (Switch) findViewById(R.id.nSettings_enabled);
        boolean b = Boolean.parseBoolean(notifConfig.getConfig("enabled","true"));

        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                d1.setEnabled(isChecked);
                d3.setEnabled(isChecked);
                w1.setEnabled(isChecked);
                w2.setEnabled(isChecked);

                if(!isChecked){
                    d1.setChecked(isChecked);
                    d3.setChecked(isChecked);
                    w1.setChecked(isChecked);
                    w2.setChecked(isChecked);
                }

                notifConfig.setConfig("enabled",isChecked);
                notifConfig.save();

            }
        });

        sw.setChecked(b);
        if(!b){
            d1.setChecked(b);
            d3.setChecked(b);
            w1.setChecked(b);
            w2.setChecked(b);
        }
    }

    @Override
    public void onBackPressed() {
        reloadService();
        super.onBackPressed();
    }
}
