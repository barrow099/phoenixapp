package hu.barrow099.phoenixapp.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import hu.barrow099.phoenixapp.R;

public class CrashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash);
        final TextView txt = (TextView)findViewById(R.id.crashText);
        txt.setMovementMethod(new ScrollingMovementMethod());
        if(getIntent().getExtras().containsKey("text"))
            txt.setText(getIntent().getExtras().getString("text"));
        Button b = (Button) findViewById(R.id.exit);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });
        Button b2 = (Button) findViewById(R.id.copy);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"phoenixapp@barrow099.hu"});
                i.putExtra(Intent.EXTRA_SUBJECT, "Hibajelentés");
                i.putExtra(Intent.EXTRA_TEXT   , txt.getText());
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(CrashActivity.this, "Nincsen telepített email kliens", Toast.LENGTH_SHORT).show();
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("crashReport", txt.getText());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getApplicationContext(),"Másolva a vágólapra",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        System.exit(-1);
    }
}
