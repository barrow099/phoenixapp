package hu.barrow099.phoenixapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.HashMap;
import java.util.Map;

import hu.barrow099.phoenixapp.Config;
import hu.barrow099.phoenixapp.R;
import hu.barrow099.phoenixapp.data.MeccsManager;

public class SettingsActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                overridePendingTransition(R.anim.rev_slide_in, R.anim.rev_slide_out);
                return true;
            default: return true;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        createListContents();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }


    private void addListIntents(){
        Intent intent = new Intent(SettingsActivity.this,KedvencekActivity.class);
        Config config = new Config("kedvencek");
        config.load();
        intent.putExtra("config",config);
        intent.putExtra("csapatok", MeccsManager.getInstance().getCsapatLista());

        settingsMap.put("Kedvencek",intent);

        Intent intent2 = new Intent(SettingsActivity.this,NotificationSettingsActivity.class);

        settingsMap.put("Értesítések",intent2);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.rev_slide_in, R.anim.rev_slide_out);
    }

    private Map<String,Intent> settingsMap;
    private void createListContents() {
        settingsMap = new HashMap<>();
        addListIntents();

        ListView lv = (ListView) findViewById(R.id.settings_sList);
        String[] keys = new String[settingsMap.keySet().size()];
        keys = settingsMap.keySet().toArray(keys);
        ArrayAdapter<String> a = new ArrayAdapter<>(this,R.layout.simple_list_item,keys);
        lv.setAdapter(a);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String key = (String) parent.getItemAtPosition(position);
                Intent i = settingsMap.get(key);
                if(i != null)
                    startActivity(i);
            }
        });
    }
}


























