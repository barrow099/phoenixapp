package hu.barrow099.phoenixapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import hu.barrow099.phoenixapp.Config;
import hu.barrow099.phoenixapp.Log;
import hu.barrow099.phoenixapp.R;
import hu.barrow099.phoenixapp.data.CsapatAdapter;
import hu.barrow099.phoenixapp.data.model.Csapat;
import hu.barrow099.phoenixapp.data.model.MeccsLista;
import hu.barrow099.phoenixapp.notification.NotificationManager;

public class KedvencekActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        setContentView(R.layout.activity_kedvencek);
        if(getIntent().getExtras() != null){
            String[] data = getIntent().getExtras().getStringArray("csapatok");
            final Config cfg = (Config) getIntent().getExtras().get("config");

            cfg.load();
            cfg.setConfig("kedvenc1",null);
            cfg.setConfig("kedvenc2",null);
            cfg.setConfig("kedvenc3",null);
            ArrayList<Csapat> csl = new ArrayList<>();
            int index = 0;
            for (String s : data) {
                if(s.equals("Összes") || s.equals("Lejátszott") || s.equals("Kedvencek"))
                    continue;
                csl.add(new Csapat(s,false));
            }
            if(csl.isEmpty()){
                cfg.setConfig("kedvencek","true");
                cfg.save();
                new NotificationManager().updateNotificationsForChangedFav();
                MainActivity.remakeList();
                overridePendingTransition(R.anim.rev_slide_in, R.anim.rev_slide_out);
                finish();
                overridePendingTransition(R.anim.rev_slide_in, R.anim.rev_slide_out);
            }
            Csapat[] css = new Csapat[csl.size()];
            css = csl.toArray(css);
            ListView lv = (ListView) findViewById(R.id.kedvencek_csapatlist);
            final CsapatAdapter csa = new CsapatAdapter(lv.getContext(),R.layout.csapatlist_row,css);
            lv.setAdapter(csa);
            final Button b = (Button)findViewById(R.id.kedvencek_okbutton);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Csapat[] data1 = csa.data;
                    int index = 1;
                    for (Csapat csapat : data1) {
                        if(csapat.isSelected()){
                            cfg.setConfig("kedvenc" + index++,csapat.getName());
                        }
                    }cfg.setConfig("kedvencek","true");
                    cfg.save();
                    new NotificationManager().updateNotificationsForChangedFav();
                    MainActivity.remakeList();
                    overridePendingTransition(R.anim.rev_slide_in, R.anim.rev_slide_out);
                    finish();
                    overridePendingTransition(R.anim.rev_slide_in, R.anim.rev_slide_out);
                }
            });

        }else{
            throw new IllegalStateException("Nem lett csapatLista és Config küldve");
        }
    }

    private void showWarn(View v) {
        AlertDialog alertDialog = new AlertDialog.Builder(v.getContext()).create();
        alertDialog.setTitle("Hiba");
        alertDialog.setMessage("Legalább egy kedvenc csapatot választania kell");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Rendben",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        //Nothing
    }
}
