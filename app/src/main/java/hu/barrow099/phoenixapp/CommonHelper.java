package hu.barrow099.phoenixapp;

/**
 * Created by Barrow099 on 2016. 11. 03..
 */

public class CommonHelper {
    public static void sleep(int millis){
        try{
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
