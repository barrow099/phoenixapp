package hu.barrow099.phoenixapp.notification;

import java.nio.charset.StandardCharsets;
import java.util.Date;

import hu.barrow099.phoenixapp.Log;
import hu.barrow099.phoenixapp.data.model.Meccs;

/**
 * Készítette Magyar Máté
 * 2016. 11. 10.
 */

public class MeccsNotification {
    private Meccs mMeccs;
    private Date showDate;
    private boolean showed;

    public MeccsNotification(Meccs mMeccs, Date showDate) {
        this.mMeccs = mMeccs;
        this.showDate = showDate;
        this.showed = false;
    }

    public String getID(){
        String s = mMeccs.getMeccsDatum().getTime() + "-" + showDate.getTime() + "-";
        byte[] bytes = mMeccs.getPhxTeam().getBytes();
        for (byte aByte : bytes) {
            s += aByte;
        }
        return s;
    }

    public Meccs getmMeccs() {
        return mMeccs;
    }

    public void setmMeccs(Meccs mMeccs) {
        this.mMeccs = mMeccs;
    }

    public Date getShowDate() {
        return showDate;
    }

    public void setShowDate(Date showDate) {
        this.showDate = showDate;
    }

    public boolean isShowed() {
        return showed;
    }

    public void setShowed(boolean showed) {
        this.showed = showed;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof MeccsNotification)){
            Log.w("Cast error");
            return false;
        }

        MeccsNotification mn = (MeccsNotification) obj;
        Log.d("ID COMAPRE: " + this.getID() + " || " + mn.getID());
        return (mn.getID().equals(this.getID()));
    }
}
