package hu.barrow099.phoenixapp.notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import hu.barrow099.phoenixapp.Config;
import hu.barrow099.phoenixapp.DebugConfig;
import hu.barrow099.phoenixapp.Log;
import hu.barrow099.phoenixapp.R;
import hu.barrow099.phoenixapp.activity.MainActivity;
import hu.barrow099.phoenixapp.data.MeccsManager;
import hu.barrow099.phoenixapp.data.model.Meccs;
import hu.barrow099.phoenixapp.data.model.MeccsLista;

/**
 * Készítette Magyar Máté
 * 2016. 11. 10.
 */

public class NotificationManager {

    public NotificationManager() {
        //Setup code here
    }

    public void updateNotificationsForChangedFav(){
        NotificationStorage s = new NotificationStorage();
        s.deleteNotificatios();
        createNotificationsforFavourites();
    }

    public void createNotificationsforFavourites(){
        MeccsManager meccsManager = MeccsManager.getInstance();
        if(meccsManager == null){
            Log.w("El kell indítani az applikációt az értesítések létrehozásához");
            return;
        }
        Config notifConfig = new Config("notifications");
        notifConfig.load();
        if(!Boolean.parseBoolean(notifConfig.getConfig("enabled","true"))){
            Log.i("Nincsenek engedélyezve az értesítések");
            return;
        }
        Map<String, MeccsLista> meccsMap = meccsManager.getMeccsMap();
        if(!meccsMap.containsKey("Kedvencek")){
            Log.w("Nincsenek kedvencek");
            return;
        }

        ArrayList<Meccs> kedvList = meccsMap.get("Kedvencek").getMeccsek();
        NotificationStorage s = new NotificationStorage();
        ArrayList<MeccsNotification> notifications = s.getNotifications();

        boolean d1 = Boolean.parseBoolean(notifConfig.getConfig("notifday1","false"));
        boolean d3 = Boolean.parseBoolean(notifConfig.getConfig("notifday3","true"));
        boolean w1 = Boolean.parseBoolean(notifConfig.getConfig("notifweek1","false"));
        boolean w2 = Boolean.parseBoolean(notifConfig.getConfig("notifweek2","false"));



        for (Meccs meccs : kedvList) {
            if(d1){
                createNotifForDate(1,meccs,notifications,s);
            }
            if(d3){
                createNotifForDate(3,meccs,notifications,s);
            }
            if(w1){
                createNotifForDate(7,meccs,notifications,s);
            }
            if(w2){
                createNotifForDate(14,meccs,notifications,s);
            }
        }

    }

    private void createNotifForDate(int i,Meccs m1,ArrayList<MeccsNotification> notifications,NotificationStorage s){
        Date showDate = MeccsIdoHelper.getDateBeforeMeccs(m1,i);
        Log.d("NotifDate: " + showDate.toString());
        MeccsNotification m = new MeccsNotification(m1,showDate);
        boolean shouldSave = true;
        for (MeccsNotification notification : notifications) {
            if(notification.equals(m ))
                shouldSave = false;
        }
        if(shouldSave) {
            s.saveNotification(m);
            Log.d("Értesítés elmentve: " + i + "|| " + m.getID());
        }
        else
            Log.d("Ez mar el van mentve");
    }


     /*
                    if(meccs.getHazai().startsWith("Phoenix")){
                        text = meccs.getHazai() + " " + meccs.getPhxTeam() + " - " + meccs.getEllenfel();
                    }else{
                        text = meccs.getHazai() + " - " + meccs.getEllenfel() + " " + meccs.getPhxTeam() ;
                    }
                    */


    private String buildMeccsString(Meccs meccs){
        if(meccs.getHazai().startsWith("Phoenix")){
            return meccs.getHazai() + " " + meccs.getPhxTeam() + " - " + meccs.getEllenfel();
        }else{
            return meccs.getHazai() + " - " + meccs.getEllenfel() + " " + meccs.getPhxTeam() ;
        }
    }

    public void showNotifications(Context notificationService) {
        NotificationStorage s = new NotificationStorage();
        ArrayList<MeccsNotification> notifications = s.getNotifications();
        Date today = new Date();
        ArrayList<MeccsNotification> mn2show = new ArrayList<>();
        for(MeccsNotification mn : notifications){
            if(MeccsIdoHelper.isNotifToday(mn,today) && !mn.isShowed()){
                Log.d("Ma van: " + mn.getID());
                mn.setShowed(true);
                s.resave(mn);
                mn2show.add(mn);
            }
        }
        ArrayList<String> lineList = new ArrayList<>();
        for(MeccsNotification not : mn2show){
            lineList.add(" " + buildMeccsString(not.getmMeccs()));
            lineList.add("   Időpont: " + new SimpleDateFormat("MM.dd HH:mm").format(not.getmMeccs().getMeccsDatum()));
            lineList.add("   Helyszín: " + not.getmMeccs().getHelyszin());
            lineList.add("\n");
        }
        String[] lList = new String[lineList.size()];
        lList = lineList.toArray(lList);
        if(lineList.size() != 0)
            sendNotification(notificationService,"Meccs Értesítés",lList);

    }

    private void sendNotification(Context ctx,String header,String ... textToShow){
        String notifText = "";
        for(String s : textToShow)
            notifText += s + "\n";

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(ctx);
        mBuilder.setSmallIcon(R.drawable.notification_icon)
                .setContentTitle("Meccs értesítés")
                .setTicker(header)
                .setContentText(notifText)
                .setLargeIcon(BitmapFactory.decodeResource(ctx.getResources(),R.mipmap.logo_t))
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(notifText));

        Intent resultIntent = new Intent(ctx, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(ctx);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        android.app.NotificationManager mNotificationManager =
                (android.app.NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(2, mBuilder.build());
    }

}
