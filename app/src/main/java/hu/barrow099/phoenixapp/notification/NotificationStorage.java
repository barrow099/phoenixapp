package hu.barrow099.phoenixapp.notification;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import hu.barrow099.phoenixapp.Log;
import hu.barrow099.phoenixapp.StorageHelper;

/**
 * Készítette Magyar Máté
 * 2016. 11. 10.
 */

public class NotificationStorage {
    private File notifDir;
    public NotificationStorage() {
        notifDir = new File(StorageHelper.getAppDir(),"notif");
        StorageHelper.createDirIfNotExists(notifDir);
    }

    public ArrayList<MeccsNotification> getNotifications(){
        notifDir = new File(StorageHelper.getAppDir(),"notif");
        ArrayList<MeccsNotification> nList = new ArrayList<>();
        Gson gs = new GsonBuilder().setDateFormat("yyyy.MM.dd HH:mm").create();
        for (File file : notifDir.listFiles()) {
            try {
                BufferedReader r = new BufferedReader(new FileReader(file));
                StringBuilder text = new StringBuilder();
                String line;
                while((line = r.readLine()) != null)
                    text.append(line + "\n");
                r.close();
                Log.d(text.toString());
                nList.add(gs.fromJson(text.toString(),MeccsNotification.class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return nList;
    }
    public void saveNotification(MeccsNotification m){
        File f = new File(notifDir,m.getID() + ".json");
        Gson gs = new GsonBuilder().setDateFormat("yyyy.MM.dd HH:mm").create();
        try {
            FileWriter w = new FileWriter(f);
            w.write(gs.toJson(m,MeccsNotification.class));
            w.flush();
            w.close();
            Log.i("Notification with id \'" + m.getID() + "\' elmentve");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void resave(MeccsNotification m) {
        File f = new File(notifDir,m.getID() + ".json");
        if(f.exists())
            f.delete();
        Gson gs = new GsonBuilder().setDateFormat("yyyy.MM.dd HH:mm").create();
        try {
            FileWriter w = new FileWriter(f);
            Log.d(gs.toJson(m,MeccsNotification.class));
            w.write(gs.toJson(m,MeccsNotification.class));
            w.flush();
            w.close();
            Log.i("Notification with id \'" + m.getID() + "\' újra elmentve");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteNotificatios() {
        for(File f : notifDir.listFiles()){
            f.delete();
        }
    }
}
