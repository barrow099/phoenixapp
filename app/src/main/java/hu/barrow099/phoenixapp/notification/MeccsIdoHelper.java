package hu.barrow099.phoenixapp.notification;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import hu.barrow099.phoenixapp.data.model.Meccs;

/**
 * Készítette Magyar Máté
 * 2016. 11. 10.
 */
public class MeccsIdoHelper {

    public static Date getDateBeforeMeccs(Meccs meccs, int napok) {
        Date meccsDatum = meccs.getMeccsDatum();
        Calendar c = new GregorianCalendar();
        c.setTime(meccsDatum);
        c.add(Calendar.DATE,-napok);
        return c.getTime();
    }

    public static boolean isNotifToday(MeccsNotification mn,Date d){
        Date sd = mn.getShowDate();
        return (sd.getYear() == d.getYear() && sd.getMonth() == d.getMonth() && sd.getDate() == d.getDate());
    }
}
