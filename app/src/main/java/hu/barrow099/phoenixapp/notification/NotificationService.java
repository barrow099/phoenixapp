package hu.barrow099.phoenixapp.notification;

import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;

import java.util.Date;

import hu.barrow099.phoenixapp.Config;
import hu.barrow099.phoenixapp.Log;
import hu.barrow099.phoenixapp.PhoenixApp;
import hu.barrow099.phoenixapp.R;
import hu.barrow099.phoenixapp.activity.MainActivity;

/**
 * Készítette Magyar Máté
 * 2016. 11. 09.
 */

public class NotificationService extends Service {
    private boolean mRunning;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.i("Service stopped");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!mRunning) {
            mRunning = true;
            Log.i("Service started");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    while (true) {
                        Date d = new Date();
                        if (!PhoenixApp.alwaysShowNotif && (d.getHours() > 21 || d.getHours() < 7)) {
                            Log.i("Ilyenkor nem ertesitunk: " + d.toString());
                            try {
                                Thread.sleep(1000 * 60 * 60);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            continue;
                        }
                        NotificationManager m = new NotificationManager();
                        m.createNotificationsforFavourites();
                        Config notifConfig = new Config("notifications");
                        notifConfig.load();

                        if (Boolean.parseBoolean(notifConfig.getConfig("enabled", "true")))
                            m.showNotifications(NotificationService.this);

                        try {
                            Thread.sleep(1000 * 60 * 60);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }).start();

        }
        return START_STICKY;
    }



    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("Service created");
        mRunning = false;

    }

    private void sendNotification(String msg){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this);
                        mBuilder.setSmallIcon(R.drawable.notification_icon)
                        .setContentTitle("Phonix Notification")
                        .setContentText(msg)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.logo_t));

        Intent resultIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        android.app.NotificationManager mNotificationManager =
                (android.app.NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        try{
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        }catch (Exception e){
            e.printStackTrace();
        }
        mNotificationManager.notify(2, mBuilder.build());
    }



}
