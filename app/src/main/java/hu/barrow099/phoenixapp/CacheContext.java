package hu.barrow099.phoenixapp;

import android.content.Context;

/**
 * Created by Barrow099 on 2016. 11. 03..
 */

public class CacheContext {
    private static Context cContext;

    public static Context getContext(){
        return cContext;
    }
    public static void setContext(Context c){
        cContext = c;
    }
}
